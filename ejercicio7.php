
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>
<body>
    <?php 
        $parcial1 = rand(0,30);
        $parcial2 = rand(0,20);
        $final1 = rand(0,50);

        $total = $parcial1 + $parcial2 + $final1;

        switch (true) {
            case ($total > 0 and $total < 60) :
                echo "Su puntaje es $total y su calificación es 1";
                break;
            case ($total > 59 and $total < 70) :
                echo "Su puntaje es $total y su calificación es 2";
                break;
            case ($total > 69 and $total < 80) :
                echo "Su puntaje es $total y su calificación es 3";
                break;
            case ($total > 79 and $total < 90) :
                echo "Su puntaje es $total y su calificación es 4";
                break;
            case ($total > 89 and $total <= 100) :
                echo "Su puntaje es $total y su calificación es 5";
                break;
            
        }
    ?>

    
</body>
</html>


