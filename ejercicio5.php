
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>
<style>
    /* .title {
        background-color: yellow;
    }

    .subtitle {
        background-color: grey;
    } */

    tr:nth-child(odd) {
        background-color: white;
    }
    tr:nth-child(even) {
        background-color: grey;
    }
    
</style>



<body>
    <table style="width: 50%;">
    <caption >Tabla de multiplicar 9</caption>
    <?php    
        $num = 9;   
        for($i=1; $i<=10; $i++) { 
            $product = $i*$num;
    ?> 
            <tr>
        <td><?php  echo "$num * $i = $product" ; ?> </td>
        </tr>  
    
    <?php  
        }  
    ?> 

    </table>
    
</body>
</html>